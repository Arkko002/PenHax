#include <stdio.h>
#include <windows.h>

int ipsnatcher()
{
    char buffer[512];
    FILE *fp, *f;

    if((fp = popen("ipconfig", "r")) == NULL)
    {
        printf("Error while opening pipe");
        return 1;
    }

    if ((f = fopen("logs.txt", "a")) == NULL)
    {
        printf("Error while opening logs file");
        return 1;
    }

    while (fgets(buffer, 512, fp) != NULL)
    {
        fprintf(f, buffer);
    }

    return 0;
};

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    ipsnatcher();
    return 0;
}
